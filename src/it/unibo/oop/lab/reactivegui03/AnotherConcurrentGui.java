package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class AnotherConcurrentGui extends JFrame {

	/**
	 * 
	 */
	private final CounterAgent counterAgent = new CounterAgent();
	private final WaitAndStopAgent waitAndStopAgent = new WaitAndStopAgent();

	private static final long serialVersionUID = 1L;
	protected static final double HEIGHT_PERC = 0.1;
	protected static final double WIDTH_PERC = 0.2;

	protected final JPanel mainPanel = new JPanel();
	protected final JLabel display = new JLabel("0");
	protected final JButton up = new JButton("up");
	protected final JButton down = new JButton("down");
	protected final JButton stop = new JButton("stop");

	public AnotherConcurrentGui() {
		super();
		// Sets the default application size based on the actual Screen size
		final Dimension defaultScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (defaultScreenSize.getWidth() * AnotherConcurrentGui.WIDTH_PERC),
				(int) (defaultScreenSize.getHeight() * AnotherConcurrentGui.HEIGHT_PERC));
		// Let the OS set the frame location
		this.setLocationByPlatform(true);
		// Sets the default close operation on close
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Attaches the components
		this.getContentPane().add(mainPanel);
		this.mainPanel.add(display);
		this.mainPanel.add(up);
		this.mainPanel.add(down);
		this.mainPanel.add(stop);
		// Starts the counterAgent and WaitAndStopAgent Threads
		counterAgent.start();
		waitAndStopAgent.start();
		// Adds component's listeners
		up.addActionListener(e -> counterAgent.setReverseCount(false));
		down.addActionListener(e -> counterAgent.setReverseCount(true));
		stop.addActionListener(e -> {
			counterAgent.stopCount();
		});
		// Renders the GUI
		this.setVisible(true);
	}

	final class CounterAgent extends Thread {

		private int counter = 0;
		private volatile boolean alive = true;
		private volatile boolean decreasing = false;

		@Override
		public void run() {
			while (alive) {
				counter += decreasing ? -1 : 1;
				SwingUtilities
						.invokeLater(() -> AnotherConcurrentGui.this.display.setText(String.valueOf(this.counter)));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// Wraps the exception in an unchecked exception,
					// giving the responsibility to the caller
					// without loosing the informations in e
					throw new IllegalStateException(e);
				}
			}
		}

		public void setReverseCount(final boolean cond) {
			this.decreasing = cond;
		}

		public void stopCount() {
			this.alive = false;
			// Following operations must be done by EDT
			SwingUtilities.invokeLater(() -> up.setEnabled(false));
			SwingUtilities.invokeLater(() -> down.setEnabled(false));
			SwingUtilities.invokeLater(() -> stop.setEnabled(false));
		}

	}

	final class WaitAndStopAgent extends Thread {

		@Override
		public void run() {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// Wraps the exception in an unchecked exception,
				// giving the responsibility to the caller
				// without loosing the informations in e
				throw new IllegalStateException(e);
			}
			AnotherConcurrentGui.this.counterAgent.stopCount();
		}

	}
}
