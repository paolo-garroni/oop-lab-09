package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static final double HEIGHT_PERC = 0.1;
	protected static final double WIDTH_PERC = 0.2;

	protected final JLabel display = new JLabel("0");
	protected final JButton up = new JButton("up");
	protected final JButton down = new JButton("down");
	protected final JButton stop = new JButton("stop");

	public ConcurrentGui() {
		super();
		// Sets the default application size based on the actual Screen size
		final Dimension defaultScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int) (defaultScreenSize.getWidth() * ConcurrentGui.WIDTH_PERC),
				(int) (defaultScreenSize.getHeight() * ConcurrentGui.HEIGHT_PERC));
		// Let the OS set the frame location 
		this.setLocationByPlatform(true);
		// Sets the default close operation on close
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Attaches the components
		JPanel mainPanel = new JPanel();
		this.getContentPane().add(mainPanel);
		mainPanel.add(display);
		mainPanel.add(up);
		mainPanel.add(down);
		mainPanel.add(stop);
		// Creates the counterAgent Thread which is responsible for the counter feature
		final CounterAgent counterAgent = new CounterAgent();
		counterAgent.start();
		// Adds component's listeners
		up.addActionListener(e -> counterAgent.setReverseCount(false));
		down.addActionListener(e -> counterAgent.setReverseCount(true));
		stop.addActionListener(e -> {
			counterAgent.stopCount();
			up.setEnabled(false);
			down.setEnabled(false);
			stop.setEnabled(false);
		});
		// Renders the GUI
		this.setVisible(true);
	}

	final class CounterAgent extends Thread {

		private int counter = 0;
		private volatile boolean alive = true;
		private volatile boolean decreasing = false;

		@Override
		public void run() {
			while (alive) {
				counter += decreasing ? -1 : 1;
				SwingUtilities.invokeLater(() -> ConcurrentGui.this.display.setText(String.valueOf(this.counter)));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// Wraps the exception in an unchecked exception, 
					// giving the responsibility to the caller 
					// without loosing the informations in e
					throw new IllegalStateException(e);
				}
			}
		}

		public void setReverseCount(final boolean cond) {
			this.decreasing = cond;
		}

		public void stopCount() {
			this.alive = false;
		}

	}
}
