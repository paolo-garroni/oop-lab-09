package it.unibo.oop.lab.workers02;

import java.util.*;

/**
 * 
 * Implementation of SumMatrix that distributes between a given number 
 * of threads the computation of the sum of all elements in a matrix.
 */
public class MultiThreadedSumMatrix implements SumMatrix {

	private final int numWorkers;
	private volatile double totalSum;

	/**
	 * @param n 
	 * 		Number of threads used to distribute the sum computation
	 */
	public MultiThreadedSumMatrix(int n) {
		this.numWorkers = n;
		this.totalSum = 0;
	}

	
	/**
	 * Computes the sum of all elements in a given Matrix distributing the 
	 * computation between {@code n} number of threads, with {@code n} 
	 * specified in {@link #MultiThreadedSumMatrix(int)}.
	 */
	@Override
	public double sum(double[][] matrix) {
		int rowsOffset = 0;
		int rowsPerWorker = matrix.length / this.numWorkers + 1;
		Collection<Worker> threadsList = new ArrayList<>();
		for (int i = 1; i <= numWorkers; i++) {
			Worker t = new Worker(matrix, rowsOffset, rowsOffset + rowsPerWorker);
			threadsList.add(t);
			t.start();
			rowsOffset += rowsPerWorker;
		}
		for (Worker t : threadsList) {
			try {
				t.join();
				totalSum += t.getPartialSum();
			} catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}
		}
		return totalSum;
	}

	/**
	 * Thread that computes the sum of the elements of the matrix	
	 * in the specified range.
	 */
	public class Worker extends Thread {

		double[][] matrix;
		int lowerBound;
		int upperBound;
		double partialSum;
		
		/**
		 * @param matrix
		 * 		Matrix containing the elements to sum
		 * @param lowerBound
		 * 		Index of the first row containing elements to sum (lower bound - starting row)
		 * @param upperBound
		 * 		Index of the first row not part of the computation (tight upper bound - stopping row)
		 */
		protected Worker(double[][] matrix, int lowerBound, int upperBound) {
			super();
			this.matrix = matrix;
			this.lowerBound = lowerBound;
			this.upperBound = upperBound < matrix.length ? upperBound : matrix.length;
			this.partialSum = 0;
		}
		
		/**
		 * For all rows belonging between lowerBound (included) and upperBound (not included)
		 * sums all elements in the row.
		 */
		public void run() {
			for (int j = lowerBound; j < upperBound; j++) {
				Arrays.spliterator(matrix[j]).forEachRemaining((double n) -> this.partialSum += n);
			}
		}
		
		/**
		 * Returns the sum computed by the thread in the specified range.
		 * @return
		 * 		The sum computed by the thread in the specified range.
		 */
		public double getPartialSum() {
			return this.partialSum;
		}
	}

}
